# language: pt
# encoding: utf-8

@AcessarGit
Funcionalidade: Acessar o git

  @Login
  Cenario: Acessar o git com um login valido
    Dado que estou na tela de login do git
    Quando insiro os dados validos
    E clico en entrar
    Entao sou direcionado para pagina home

  @Login2
  Cenario: Acessar o git com um login valido
    Dado que estou na tela de login do git
    Quando insiro os dados de login "lbonomi" e senha "senha"
    E clico en entrar
    Entao sou direcionado para pagina home

  @Login3
  Esquema do Cenario: Acessar o git com um login valido
    Dado que estou na tela de login do "<url>"
    Quando insiro os dados de login "<usuario>" e senha "<senha>"
    E clico en entrar
    Entao sou direcionado para pagina home

    Exemplos: 
      | usuario  | senha  | url            |
      | lbonomi  | abc123 | www.google.com |
      | danielle | cba321 | www.fb.com     |
