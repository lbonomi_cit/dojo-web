package util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Utils {

	public static WebDriver webDriver;
	
	public WebDriver getWebDriverChrome() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\lbonomi\\eclipse-workspace\\dojo-web\\src\\main\\drivers\\chromedriver.exe");
		webDriver = new ChromeDriver();
		webDriver.manage().window().maximize();
		return webDriver;
	}

	public static WebDriver getWebDriver() {
		return webDriver;
	}

	public void setWebDriver(WebDriver webDriver) {
		this.webDriver = webDriver;
	}
}
