package screen;

import org.openqa.selenium.By;

import util.Utils;

public class AcessarGitScreen extends Utils{

	public void acessarUrlGit() {
		webDriver = getWebDriverChrome();
		webDriver.get("https://github.com/");
		
		webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div[2]/div[2]/a[1]")).click();
		setWebDriver(webDriver);
	}
	
}
