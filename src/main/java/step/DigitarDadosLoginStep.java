package step;

import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Quando;
import screen.DigitarDadosLoginScreen;

public class DigitarDadosLoginStep {
	@Quando("insiro os dados validos$")
	public void insiroDados() {
		DigitarDadosLoginScreen digitarDadosLoginScreen = new DigitarDadosLoginScreen();
		digitarDadosLoginScreen.digitarLogin();
	}

	@Quando("insiro os dados de login \"([^\"]*)\" e senha \"([^\"]*)\"$")
	public void insiroDados(String usuario, String senha) {
		
	}
	
	@E("clico en entrar$")
	public void clicarEntrar() {
		
	}
}
