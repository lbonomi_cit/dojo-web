package step;

import io.cucumber.java.pt.Dado;
import screen.AcessarGitScreen;

public class AcessarGitStep {

	@Dado("que estou na tela de login do git$")
	public void acessarGit() {
		AcessarGitScreen acessarGitPage = new AcessarGitScreen();
		acessarGitPage.acessarUrlGit();
	}

	@Dado("que estou na tela de login do \"([^\"]*)\"$")
	public void acessarSite(String url) {
		
	}
}
